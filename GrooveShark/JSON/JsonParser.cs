﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization;
namespace GroovesharkApi.JSON
{
    internal static class JsonParser
    {
        private static JsonSerializerSettings settings = new JsonSerializerSettings();
        public static T deserialize<T>(string json)
        {
            if (json.StartsWith("{\"errors\""))
            {
                dynamic resp = JsonConvert.DeserializeObject<dynamic>(json);
                throw new GroovesharkException(resp.errors[0].message.Value, (int)resp.errors[0].code.Value);
            }

            var response = JsonConvert.DeserializeObject<dynamic>(json);

            if (response.errors != null) throw new GroovesharkException(response.errors[0].message.ToObject<string>(), response.errors[0].code.ToObject<int>());
            return response.result.ToObject<T>();
        }
 
        public static string Serialize<T>(T obj)
        {
           return JsonConvert.SerializeObject(obj, typeof(T), settings);
        }

    }
}
