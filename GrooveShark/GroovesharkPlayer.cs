﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GroovesharkApi.Data;
namespace GroovesharkApi
{
    public class GroovesharkPlayer : IDisposable
    {
        public IList<Data.Song> Playlist {get; private set;}
        private IList<Data.Song> Queue { get; set;  }
        public bool Shuffle {get; set;}

        public StreamKey StreamKey { get; private set; }

        public void Dispose()
        {

        }
        
        public void Play(Song song)
        {
            Queue.Add(song);
        }
        public void Play(IList<Song> songs, bool shuffle = false)
        {
            Playlist = Playlist;
            this.Shuffle = shuffle;
        }
        public void PlayNext(Song song)
        {
            Queue.Add(song);

        }
        public void PlayNext(IList<Song> songs)
        {
            ((List<Song>)Queue).AddRange(songs);
        }

        public void AddToPlaylist(Song song)
        {
            Playlist.Add(song);
        }
        public void AddToPlaylist(IList<Song> songs)
        {
            ((List<Song>)Playlist).AddRange(songs);
        }

        public void Start()
        {

        }
        public void Stop()
        {

        }

        private void markStreamKeyOver30Secs()
        {
 
        }
        private void markSongComplete ()
        {

        }


    }
}
