﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi
{
    public class GroovesharkException : Exception
    {
        public int ID { get; private set; }
        public GroovesharkException(string Message, int ID) : base (Message)
        {   
            this.ID = ID;
        }
        public GroovesharkException(string Message, int ID, Exception inner) : base(Message,inner)
        {
            this.ID = ID;
        }
        static GroovesharkException Parse(string JSON)
        {
            //TODO: Correctly Parse
            return new GroovesharkException("ERR", 0);
        }
    }
}
