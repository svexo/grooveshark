﻿using GroovesharkApi.Data;
using GroovesharkApi.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi
{

    /*
     * TODO:
     *  Add remaining functions
     *  Refractor
     *  Async ??
     *  Test
     */

    public class Grooveshark
    {
        private string Key { get; set; }
        private string Secret { get; set; }
        public string SessionId { get; private set; }
        private header Header { get { return new header(Key, SessionId); } }
        public User AuthenticatedUser { get; private set; }
        public string CountryID { get { return countryInfo == null ? null : countryInfo["ID"]; } }
        private Dictionary<string, string> countryInfo;


        #region Helper Structs 
        [DataContract]
        private struct function
        {
            [DataMember] 
            string method;
            [DataMember]
            Dictionary<string, string> parameters;
            [DataMember]
            header header;

            public function(string method, Dictionary<string,string> parameters, header header )
            {
                this.method = method;
                this.parameters = parameters;
                this.header = header;
            }
        }
        [DataContract]
        private struct header
        {
            [DataMember]
            string wsKey;
            [DataMember (EmitDefaultValue=false)]
            string sessionID;

            public header(string wsKey, string sessionID)
            {
                this.wsKey = wsKey;
                this.sessionID = sessionID;
            }
        }
        #endregion


        public Grooveshark(string Key,string Secret)
        {
            this.Key = Key;
            this.Secret = Secret;

            dynamic res =  SendFunction<dynamic>(new function("startSession", new Dictionary<string, string>(), Header));
            SessionId = res["sessionID"].Value;
        }

        #region User Specific Functions
        public IList<T> getUserPlaylists<T>(int limit = 0) where T : Playlist
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string,string>();
            if(limit != 0) param.Add("limit",limit.ToString());

            return SendFunction<dynamic>(new function("getUserPlaylists", param, Header)).playlists.ToObject<IList<T>>();
        }
        public IList<T> getUserPlaylistsSubscribed<T>(int limit = 0) where T : Playlist
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());

            return SendFunction<dynamic>(new function("getUserPlaylistsSubscribed", param, Header)).playlists.ToObject<IList<T>>();

        }
        public IList<T> getUserFavoriteSongs<T>(int limit = 0) where T : Song
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());

            return SendFunction<dynamic>(new function("getUserFavoriteSongs", param, Header)).songs.ToObject<IList<T>>();
        }

        public bool removeUserFavoriteSongs(string[] SongIds)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("songIDs", SongIds.Aggregate((x1, x2) => x1 + "," + x2));

            return SendFunction<dynamic>(new function("removeUserFavoriteSongs", param, Header)).success.ToObject<bool>();
        }

        //TODO: Doesnt work (No specific error given)
        public void CreatePlayList(string Name, string[] ids)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();

            param.Add("name", Name);
            param.Add("songIDs", ids.Aggregate((x1, x2) => x1 + "," + x2));

            dynamic x = SendFunction<dynamic>(new function("createPlaylist", param, Header));
            if (!(bool)(x.success)) throw new Exception(x.ToString());

        }
        public void renamePlaylist(string playlistID, string name)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);
            param.Add("name", name);

            if (!SendFunction<dynamic>(new function("renamePlaylist", param, Header)).success.ToObject<bool>())
                throw new GroovesharkException("No access rights", 9);
        }


        public IList<T> getLibrarySongs<T>(out bool hasMore, int Limit = 0, int page = 0)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();

            if (Limit != 0) param.Add("limit", Limit.ToString());
            if (page != 0) param.Add("page", page.ToString());

            dynamic result = SendFunction<dynamic>(new function("getUserLibrarySongs", param, Header));
            hasMore = result.hasMore;
            return result.songs.ToObject<IList<T>>();
        }

        public IList<T> getAllLibrarySongs<T>()
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            List<T> songs = new List<T>();
            var param = new Dictionary<string, string>();
            param.Add("limit", 100.ToString());
            param.Add("page", "");
            int page = 0;
            bool hasMore = false;
            do
            {
                param["page"] = (++page).ToString();
                dynamic result = SendFunction<dynamic>(new function("getUserLibrarySongs", param, Header));
                songs.AddRange(result.songs.ToObject<IList<T>>());
                hasMore = result.hasMore;

            } while (hasMore);
            return songs;
        }
        public void logout()
        {
            if (AuthenticatedUser == null) return;
            var param = new Dictionary<string, string>();
            SendFunction<dynamic>(new function("logout", param, Header));
        }

        public User getUserInfo()
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            return SendFunction<User>(new function("getUserLibrarySongs", param, Header));
        }

        public object getUserSubscriptionDetails() { throw new NotImplementedException(); }
        
        public bool addUserFavoriteSong(string songID)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("songID", songID);

            return SendFunction<dynamic>(new function("addUserFavoriteSong", param, Header)).success.ToObject<bool>();
        }

        public bool subscribePlaylist(string playlistID)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);

            return SendFunction<dynamic>(new function("subscribePlaylist", param, Header)).success.ToObject<bool>();
        }
        public bool unsubscribePlaylist(string playlistID)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);

            return SendFunction<dynamic>(new function("unsubscribePlaylist", param, Header)).success.ToObject<bool>();
        }

        public void deletePlaylist(string playlistID)
        {
             if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);
            var result = SendFunction<dynamic>(new function("deletePlaylist", param, Header));
            if (!result.success.ToObject<bool>()) throw new GroovesharkException("No access rights", 9);

        }
        public void unDeletePlaylist(string playlistID)
        {
            if (AuthenticatedUser == null) throw new GroovesharkException("User auth required", 100);
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);
            var result = SendFunction<dynamic>(new function("undeletePlaylist", param, Header));
            if (!result.success.ToObject<bool>()) throw new GroovesharkException("No access rights", 9);
        }


        #endregion


        /// <summary>
        /// This method wil authenticate a user with Grooveshark
        /// </summary>
        /// <param name="User">the username</param>
        /// <param name="Password">The password (Must be an MD5 hash)</param>
        public bool Authenticate(string User, string Password)
        {
            //TODO: Maybe OAUTH????
            var param = new Dictionary<string, string>();
            param.Add("login", User);
            param.Add("password", Password);
            dynamic res = SendFunction<dynamic>(new function("authenticate", param, Header));
            if (res.success.ToObject<bool>()) AuthenticatedUser = res.ToObject<User>();
            return res.success.ToObject<bool>();
        }
        public void getCountry(string IP = null)
        {
            var param = new Dictionary<string, string>();
            param.Add("ip", IP);
            this.countryInfo = SendFunction<dynamic>(new function("getCountry", param, Header)).ToObject<Dictionary<string, string>>();


        }

        public string getUserIDFromUsername(string name)
        {
            var param = new Dictionary<string, string>();
            param.Add("username", name);

            return SendFunction<dynamic>(new function("getUserIDFromUsername", param, Header)).UserID.ToObject<string>();
        }

        public T getPlaylist<T>(Playlist pl, int limit = 0) where T : Data.Playlist { return getPlaylist<T>(pl.ID, limit); }
        public T getPlaylist<T>(string Id, int limit = 0) where T : Data.Playlist
        {
            var param = new Dictionary<string, string>();
            param.Add("playlistID", Id);
            if (limit > 0) param.Add("limit ",limit.ToString());
            return SendFunction<T>(new function("getPlaylist", param, Header));
        }
         public void getPlaylistSongs(Playlist list, int limit = 0) 
        {
            list.Songs = getPlaylistSongs<Song>(list.ID);
        }
        public IList<T> getPlaylistSongs<T>(string playlistID, int limit = 0) where T : Song
        {
            var param = new Dictionary<string, string>();
            param.Add("playlistID", playlistID);
            return SendFunction<dynamic>(new function("getPlaylistSongs", param, Header)).songs.ToObject<IList<T>>();
        }

        public T getPlayListInfo<T>(string Id) where T : Data.Playlist
        {
            var param = new Dictionary<string, string>();
            param.Add("playlistID", Id);
            return SendFunction<T>(new function("getPlaylistInfo", param, Header));
        }


        public IList<T> getAlbumSongs<T>(Album album, int limit = 0) where T : Data.Song { return getAlbumSongs<T>(album.ID.ToString(), limit); }
        public IList<T> getAlbumSongs<T> (string Id, int limit = 0) where T : Data.Song
        {
            var param = new Dictionary<string, string>();
            param.Add("albumID", Id);
            if (limit > 0) param.Add("limit ", limit.ToString());

            return SendFunction<dynamic>(new function("getAlbumSongs", param, Header)).songs.ToObject<IList<T>>();
        }

        public T getAlbum<T> (string Id) where T : Data.Album
        {
            var param = new Dictionary<string, string>();
            param.Add("albumIDs", Id);
            return SendFunction<dynamic>(new function("getAlbumsInfo", param, Header)).albums.ToObject<IList<T>>()[0];

        }
        //Test this 
        public IList<T> getAlbums<T>(string[] ids) where T : Data.Album
        {
            var param = new Dictionary<string, string>();
            param.Add("albumIDs", ids.Aggregate((x1, x2) => x1 + "," + x2));
            return SendFunction<dynamic>(new function("getAlbumsInfo", param, Header)).albums.ToObject<IList<T>>();
        }

        public IList<T> getArtistAlbums<T>(Artist artist, bool onlyVerfied = true) where T : Album { return getArtistAlbums<T>(artist.ID,onlyVerfied); }
        public IList<T> getArtistAlbums<T>(string artistID, bool onlyVerfied = true) where T : Album
        {
            var param = new Dictionary<string, string>();
            param.Add("artistID", artistID);
            return SendFunction<dynamic>(new function(onlyVerfied ? "getArtistVerifiedAlbums" : "getAlbumsInfo", param, Header)).albums.ToObject<IList<T>>();
        }

        public bool getDoesSongExist(string songID)
        {
            var param = new Dictionary<string, string>();
            param.Add("songID", songID);
            return SendFunction<bool>(new function("getDoesSongExist", param, Header));
        }
        public bool getDoesAlbumExist(string albumID)
        {
            var param = new Dictionary<string, string>();
            param.Add("albumID", albumID);
            return SendFunction<bool>(new function("getDoesAlbumExist", param, Header));
        }

        public bool getDoesArtistExist(string artistID)
        {
            var param = new Dictionary<string, string>();
            param.Add("artistID", artistID);
            return SendFunction<bool>(new function("getDoesArtistExist", param, Header));
        }

        public IList<T> getArtistPopularSongs<T>(Artist artist) where T : Song { return getArtistPopularSongs<T>(artist.ID); }
        public IList<T> getArtistPopularSongs<T>(string artistID) where T : Song 
        {
            var param = new Dictionary<string, string>();
            param.Add("artistID", artistID);
            return SendFunction<dynamic>(new function("getArtistPopularSongs", param, Header)).songs.ToObject<IList<T>>();
        }
        public IList<T> getPopularSongsMonth<T>(int limit = 0) where T : Song
        {
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());

            return SendFunction<dynamic>(new function("getPopularSongsMonth", param, Header)).songs.ToObject<IList<T>>();
        }

        public IList<T> getPopularSongsToday<T>(int limit = 0) where T : Song
        {
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());

            return SendFunction<dynamic>(new function("getPopularSongsToday", param, Header)).songs.ToObject<IList<T>>();
        }

        //public IList<Song> getSongSearchResults(string query, int limit = 0, int offset = 0) { return getSongSearchResults<Song>(query, limit, offset); }
        public IList<T> getSongSearchResults<T>(string query, int limit = 0, int offset = 0) where T : Song
        {
            
            if(countryInfo == null)    
                throw new GroovesharkException("Missing parameter",4, new Exception("Country info missing. Use getCountry() to retrieve country info"));
            
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());
            if (offset != 0) param.Add("offset", offset.ToString());
            param.Add("query", query);
            param.Add("country",String.Join(",", countryInfo.Select(kvp => kvp.Key + ":" + kvp.Value)));      
            return SendFunction<dynamic>(new function("getSongSearchResults", param, Header)).songs.ToObject<IList<T>>();
        }
        public IList<T> getArtistSearchResults<T>(string query, int limit = 0) where T : Artist
        {
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());
            param.Add("query", query);

            return SendFunction<dynamic>(new function("getArtistSearchResults", param, Header)).artists.ToObject<IList<T>>();
        }
         public IList<T> getPlaylistSearchResults<T>(string query, int limit = 0) where T : Playlist
        {
            var param = new Dictionary<string, string>();
            if (limit != 0) param.Add("limit", limit.ToString());
            param.Add("query", query);

            return SendFunction<dynamic>(new function("getPlaylistSearchResults", param, Header)).playlists.ToObject<IList<T>>();
        }
         public IList<T> getAlbumSearchResults<T>(string query, int limit = 0) where T : Album
         {
             var param = new Dictionary<string, string>();
             param.Add("query", query);
             if (limit != 0) param.Add("limit", limit.ToString());

             return SendFunction<dynamic>(new function("getAlbumSearchResults", param, Header)).albums.ToObject<IList<T>>();
         }


         public IList<T> getSimilarArtists<T>(Artist artist, out bool hasNexPage, int limit = 0, int page = 0) where T : Artist
         { return getSimilarArtists<T>(artist.ID, out hasNexPage, limit, page); }
         public IList<T> getSimilarArtists<T>(string ArtistID, out bool hasNexPage, int limit = 0, int page = 0) where T : Artist
         {
             var param = new Dictionary<string, string>();

             if (limit != 0) param.Add("limit", limit.ToString());
             if (page != 0) param.Add("page", page.ToString());
             param.Add("artistID", ArtistID);

             dynamic result = SendFunction<dynamic>(new function("getSimilarArtists", param, Header)).artists;
             hasNexPage = result.pager.hasNextPage.ToObject<bool>();
             return result.artists.ToObject<IList<T>>();

         }

         public StreamKey getStreamKeyStreamServer(Song song) { return getStreamKeyStreamServer(song.ID.ToString()); }
         public StreamKey getStreamKeyStreamServer(string songID)
         {
             if (countryInfo == null)
                 throw new GroovesharkException("Missing parameter", 4, new Exception("Country info missing. Use getCountry() to retrieve country info"));
             var param = new Dictionary<string, string>();
             param.Add("songID", songID);
            //param.Add("country", "\"" + String.Join(",", countryInfo.Select(kvp => "\"" + kvp.Key + "\":\"" + kvp.Value + "\"")) + "\"");
             param.Add("country", "\"ID\":\"223\",\"CC1\":\"0\",\"CC2\":\"0\",\"CC3\":\"0\",\"CC4\":\"1073741824\",\"IPR\":\"82\"");
             return SendFunction<StreamKey>(new function("getStreamKeyStreamServer", param, Header));
         }


        private T SendFunction<T>(function func)
        {
           string data = JSON.JsonParser.Serialize<function>(func);
           string signature = null;
           Encoding encoding = Encoding.UTF8;
           var keyByte = encoding.GetBytes(Secret);


            using (var hmacsha256 = new HMACMD5(keyByte))
            {
                hmacsha256.ComputeHash(encoding.GetBytes(data));
                signature = Utilities.ByteToString(hmacsha256.Hash).ToLower();
            }


            WebRequest request = WebRequest.Create("https://api.grooveshark.com/ws/3.0/?sig=" + signature);
            request.Method = "POST";
            using(var stream = request.GetRequestStream())
            {
                byte[] requestData = encoding.GetBytes(data);
                stream.Write(requestData, 0, requestData.Length);
            }

            WebResponse response = request.GetResponse();
            using(var streamReader = new System.IO.StreamReader(response.GetResponseStream()))
            {
                data = streamReader.ReadToEnd();
            }

            return JSON.JsonParser.deserialize<T>(data);

        }

    }
}
