﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract]
    public class User
    {
        [DataMember()]
        public string UserID { get; set; }
        [DataMember(Name="FName")]
        public string FirstName { get; set; }
        [DataMember(Name="LName")]
        public string LastName { get; set; }
        [DataMember()]
        public bool IsPremium { get; set; }
        [DataMember()]
        public bool IsPlus { get; set; }
        [DataMember()]
        public bool IsAnywhere { get; set; }
    }
}
