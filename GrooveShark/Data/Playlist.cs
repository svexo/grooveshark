﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract]
    public class Playlist
    {
        [DataMember(Name = "PlaylistName")]
        public string Name { get; private set; }
        [DataMember(Name = "PlaylistID")]
        public string ID { get; private set; }
        [DataMember(Name = "UserID")]
        public int UserID { get; private set; }
        [DataMember(Name = "PlaylistDescription")]
        public string Description { get; private set; }
        [DataMember(Name = "Songs")]
        public IList<Song> Songs { get; set; }




    }
}
