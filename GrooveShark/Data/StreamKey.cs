﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract]
    public class StreamKey
    {
        [DataMember(Name="StreamKey")]
        public string Key { get; private set; }
        [DataMember(Name="url")]
        public string Url { get; private set; }
        [DataMember()]
        public string StreamServerID { get; private set; }
        [DataMember()]
        public long uSec { get; private set; }
    }
}
