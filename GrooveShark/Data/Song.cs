﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract]
    public class Song
    {
        // THIS IS MADNESS
        public string Title { get { return songNameA == null ? songNameB : songNameA; } }

        [DataMember(Name = "SongName")]
        private string songNameA;
        [DataMember(Name = "Name")]
        private string songNameB;
        [DataMember(Name="SongID")]
        public int ID { get; set; }
        [DataMember(Name = "ArtistID")]
        public int ArtistID { get; set; }
        [DataMember(Name = "ArtistName")]
        public string ArtistName { get; set; }
        [DataMember(Name = "AlbumName")]
        public string Album { get; set; }
        [DataMember(Name = "AlbumID")]
        public int AlbumID { get; set; }

    }
}
