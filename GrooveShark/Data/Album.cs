﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract]
    public class Album
    {
        [DataMember(Name="AlbumID")]
        public int ID { get; set; }
        [DataMember(Name = "AlbumName")]
        public string Name { get; set; }
        [DataMember(Name = "ArtistID")]
        public int ArtistID { get; set; }
        [DataMember(Name = "ArtistName")]
        public string ArtistName { get; set; }

        //TODO:      [CoverArtFilename]  [IsVerified]
                
    }
}
