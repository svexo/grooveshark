﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace GroovesharkApi.Data
{
    [DataContract()]
    public class Artist
    {
        [DataMember(Name="ArtistID")]
        public string ID { get; set; }
        [DataMember(Name = "ArtistName")]
        public string Name { get; set; }
        [DataMember()]
        public bool IsVerified { get; set; }
    }
}
